from quizlet import api as api
from quizlet import data_handler as dh


'''=========================================================
                    Script
========================================================='''

if __name__ == "__main__":
    print('1. Go to:')
    print('\t','https://quizlet.com')
    print('2. Login')
    print('3. Follow this Link:')
    print('\t', api.grant_link())
    print('4. Click on "Allow"')
    print('5. You will be redirected to a page with a code')
    code = input('6. paste the code here: ')
    [access_token, user] = api.get_access_token(code)
    dh.store_json('access_token.json',{
        'user': user,
        'access_token': access_token
    })
