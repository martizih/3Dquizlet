# 3DQuizlet

Quizlet API-Wrapper for managing your learnsets from a simple excel table

## Usage
You can define your vocabulary in **vocabulary.csv**. 
The vocabulary can be separated into chapters by adding a tag in the first column.
You can define your learnsets in **learnsets.json**,
simply give it a name and specify which vocabulary chapters shall be a part of the learnset.
You can use regular expressions in specifying the chapters you want.
Once you're happy with your learnset configuration you can update your quizlet learnsets with the new configuration.

##### To consider when using
* All generated learnsets are prefixed by `pyset`
* Only sets that have modified content/title will be updated


## 3DQuizlet for Users

##### Initialization
In order for 3DQuizlet to be able to upload learnsets on your behalf, you have to grant it access
to your Quizlet-learnsets by providing it with an access-token.
There is a script that leads you through the required steps and stores
the access_token in a file for further usage. This token is valid for 10 years,
so you only need to perform these steps once. Simply run `get_access_token.exe` and
follow its instructions.

##### Updating
In order to update the learnsets on your Quizlet account run:  
`3DQuizlet.exe`

## 3DQuizlet for Developers

##### Installation
1. download and install python3.* (>= python3.6) (https://www.python.org/downloads/)
2. download 3DQuizlet:
  * either via Git: `git clone https://gitlab.com/martizih/3Dquizlet/`
  * or as Zip: https://gitlab.com/martizih/3Dquizlet/repository/master/archive.zip
3. `python3 quizlet/setup.py install --user`

##### Initialization
In order for 3DQuizlet to be able to upload learnsets on your behalf, you have to grant it access to your Quizlet-learnsets by providing it with an access-token. There is a script that leads you through the required steps and stores the access_token in a file for further usage. This token is valid for 10 years, so you only need to perform these steps once.    
`python3 get_access_token.py`  

##### Updating
In order to update the learnsets on your Quizlet account run:  
`python3 update_learnsets.py`  
In order for the update process to be faster you can enable usage of 
multiple processes by setting `multi_proc = True` inside the script.
