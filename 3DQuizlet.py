import string
import multiprocessing
import re
import json
from functools import partial
from quizlet import func_helpers as fh
from quizlet import api as api
from quizlet import data_handler as dh


printable = set(string.printable)
dummy = False
character_limit = 50

forbidden = {
    '  ':' ',
    '…':'...'
}

no_braces = {
    '(':'',
    ')':''
}

'''=========================================================
                    Functions
========================================================='''

def prepare_data_item(d):
    d = {k:sanitize_string(v, forbidden) for k,v in d.items()}
    d['Pinyin (English)'] = '{} ({})'.format(
        d['Pinyin'],
        sanitize_string(d['English'], no_braces)
    )
    return d

def prepare_chapter(data):
    return [prepare_data_item(d) for d in data if d['Hanzi']]

def sanitize_string(x, to_remove):
    for src,trg in to_remove.items():
        x = x.replace(src, trg)
    return x

def count_hanzi(res):
    res = [x['Hanzi'] for x in res]
    res = [y for x in res for y in list(x)]
    res = [x for x in res if x not in printable]
    res = set(res)
    res = list(res)
    res = len(res)
    return res

def unfold_set(x):
    return {**x,
        'terms': [d['term'] for d in x['data']],
        'defs': [d['def'] for d in x['data']],
        'lterms': x['terms']['language_code'],
        'ldefs': x['defs']['language_code'],
    }



def select_data(chapter_regex, data):
    def chapter_is_selected(x):
        return any(re.fullmatch(r, x) for r in chapter_regex)
    res = [v for k,v in data.items() if chapter_is_selected(k)]
    return [y for x in res for y in x]

def _diff(local, remote, hash_local, hash_remote):
    local = fh.groupBy(local, hash_local)
    remote = fh.groupBy(remote, hash_remote)
    loc = set(local.keys())
    rem = set(remote.keys())
    only_local = loc - rem
    only_remote = rem - loc
    to_create = [local[x][0] for x in only_local]
    to_delete = [remote[x][0] for x in only_remote]   
    on_both_ends = loc & rem
    to_update = [(local[x][0], remote[x][0]) for x in on_both_ends]
    return {
        'to_create': to_create,
        'to_delete': to_delete,
        'to_update': to_update
    }

def diff_data(local_set, remote_set):
    return _diff(
        local_set['data'],
        remote_set['terms'],
        lambda x: x['term'] + x['def'],
        lambda x: x['term'] + x['definition']
    )

def diff_sets(local, remote):
    return _diff(
        local,
        remote,
        lambda x: x['title'],
        lambda x: x['title']
    )

'''=========================================================
                    Script
========================================================='''

if __name__ == "__main__":

    print('1. Load Data')
    lsets = dh.load_json('learnsets.json')
    types = dh.load_json('learnset_types.json')
    vocabulary = dh.load_json('vocabulary.json')
    data = [ d for f in vocabulary for d in dh.load_csv(f)]

    data = fh.splitBy(data, lambda x: x['Kapitel'])
    data = {k: prepare_chapter(v) for k,v in data.items()}

    lsets = [{**ls,
        'name': ls['name'] if 'name' in ls else ' '.join(ls['chapters']),
        'types': ls['types'] if 'types' in ls else ':',
    } for ls in lsets]

    lsets = [{**ls,
        'title': ' '.join(['pyset', t, ls['name']]),
        'terms': types[t]['terms'],
        'defs': types[t]['definitions']
    } for ls in lsets for t in ls['types']]

    lsets = [{**ls,
        'data': select_data(ls['chapters'], data)
    } for ls in lsets]

    print('2. Validate Number of characters used')
    too_long = [x for x in lsets if count_hanzi(x['data']) >= character_limit]
    too_long = [x['title'] for x in too_long]
    [print('the', x, 'set has too many characters') for x in too_long]

    lsets = [{**ls,
        'data': [{
            'term': d[ls['terms']['field']],
            'def': d[ls['defs']['field']]
        } for d in ls['data']]
            } for ls in lsets]

    if not dummy:
        print('3. Get Quizlet Server Version')
        login = dh.load_json('access_token.json')
        [user, access_token] = [login['user'], login['access_token']]

        sets = api.getUserInfo(access_token, user)['sets']
        sets = [api.getSet(access_token, x['id'], timestamp=x['modified_date']) for x in sets]

        sets = [s for s in sets if 'pyset' in s['title']]

        print('4. Diff Quizlet Server Version with the Local Version')
        x = diff_sets(lsets, sets)
        to_create = x['to_create']
        to_delete = x['to_delete']
        update_info = {r['id']: diff_data(l, r) for l,r in x['to_update']}
        terms_to_create = {k:v['to_create'] for k,v in update_info.items()}
        terms_to_delete = {k:v['to_delete'] for k,v in update_info.items()}
        terms_to_create = {k:v for k,v in terms_to_create.items() if len(v) > 0}
        terms_to_delete = {k:v for k,v in terms_to_delete.items() if len(v) > 0}

        print('5. Update Quizlet Server')
        [api.deleteSet(
            access_token,
            x['title'],
            x['id']
        ) for x in to_delete]

        to_create = [unfold_set(x) for x in to_create]
        [api.createSet(
            access_token,
            x['title'],
            x['terms'],
            x['defs'],
            x['lterms'],
            x['ldefs'],
        ) for x in to_create]

        [api.deleteTerm(
            access_token,
            k,
            x['id']
        ) for k,v in terms_to_delete.items() for x in v]

        [api.addTerm(
            access_token,
            k,
            x['term'],
            x['def']
        ) for k,v in terms_to_create.items() for x in v]
