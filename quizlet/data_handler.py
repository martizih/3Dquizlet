'''=========================================================
    Data Handler
        creator: martizih
        date: 13.10.2017
        reason:
            abstract data access away, so users
            no longer have to care about reading
            in csv files and dropping empty lines etc
========================================================='''

import csv
import json

def store_json(path, dict):
    with open(path, 'w+', encoding='UTF-8-sig') as fh:
        json.dump(dict, fh)

def load_json(path):
    with open(path, 'r', encoding='UTF-8-sig') as fh:
        data = json.load(fh)
    return data

def load_csv(path):
    dict_list = []
    with open(path, 'r', encoding='UTF-8-sig') as fh:
        reader = csv.DictReader(fh,delimiter=',')
        [dict_list.append(l) for l in reader]
    dict_list = [{key : value.strip()
        for key, value in d.items() if key}
            for d in dict_list]
    return dict_list