'''=========================================================
    Func Helpers
        creator: martizih
        date: 13.10.2017
        reason:
            provide some common array manipulation
            functions that tedious or not at all
            implemented in python
========================================================='''

from itertools import groupby,tee
from functools import partial

def rm_key(d, key):
    '''
    :param d: dictionary containing key
    :param key:
    :return: dictionary without key
    '''
    return {k:v for k,v in d.items() if k not in key}

def splitBy(list, key):
    '''
    :param list:
    :param key: callback for determining sublist boundaries
    :return: list of lists split whenever key yielded true
    '''
    idx = [i for i, d in enumerate(list) if key(d)]
    idx2 = [*idx[1:],len(list)]
    return {key(list[i]):list[i:j] for i, j in zip(idx, idx2)}

def split(list, key):
    '''
    :param list:
    :param key: callback for determining which sublist an item belongs
    :return: two list inside and outside
    '''
    return tee((key(item), item) for item in list)

def groupBy(list, key):
    '''
    :param list:
    :param key: callback to for determining group
    :return: dict of lists assigned into groups
    '''
    return {key:[v for v in val]
            for key,val in groupby(list, key)}
