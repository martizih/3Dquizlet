'''=========================================================
    API Wrapper
        creator: martizih
        date: 13.10.2017
        reason:
            Wrap the quizlet API so we can abstract
            away from actual get/posts and rather
            think about creating and deleting sets
========================================================='''

import requests
from urllib.parse import urlencode
import pprint
import json
import os

def memoize(file_name):
    def decorator(original_func):
        try:
            cache = json.load(open(file_name, 'r'))
        except (IOError, ValueError):
            cache = {}
        def new_func(*args, **kwargs):
            key = json.dumps(args) + json.dumps(kwargs)
            if key not in cache:
                cache[key] = original_func(*args, **kwargs)
                json.dump(cache, open(file_name, 'w'))
            return cache[key]
        return new_func
    return decorator


client_id = '3ttPQKQuwY'
client_secret = 'kDbQfJrDQBWVPmsC2c9y4C'

def grant_link():
    request = 'https://quizlet.com/authorize?' + urlencode({
        'client_id': client_id,
        'response_type': 'code',
        'scope': 'read write_set',
        'state': 'random_randy'
    })
    return request

def get_access_token(code):
    response = requests.post(
        "https://api.quizlet.com/oauth/token",
        data = {
            'grant_type': 'authorization_code',
            'code': code,
            'redirect_uri': 'https://3dquizlet.000webhostapp.com/'
        },
        auth = (
            client_id,
            client_secret
        )
    )
    _check_status(response, code=200)
    payload = response.json()
    return [payload['access_token'], payload['user_id']]

@memoize('getSet.dat')
def getSet(access_token, set_id, timestamp=None):
    print('loading', str(set_id))
    return _get(
        access_token,
        'https://api.quizlet.com/2.0/sets/' + str(set_id),
        code = 200
    )

def getUserInfo(access_token, user):
    return _get(
        access_token,
        'https://api.quizlet.com/2.0/users/' + user,
        code = 200
    )

def deleteSet(access_token, title, id):
    print('deleting:', title)
    _delete(
        access_token,
        'https://api.quizlet.com/2.0/sets/' + str(id),
        code = 204
    )


def createSet(access_token, title, terms, defs, lterms, ldefs):
    print('creating:', title)
    _post(
        access_token,
        'https://api.quizlet.com/2.0/sets',
        data = {
            'title': title,
            'terms': terms,
            'definitions': defs,
            'lang_terms': lterms,
            'lang_definitions': ldefs,
            'visibility': 'public'
        },
        code = 201
    )

def addTerm(access_token, set_id, term, definition):
    print('adding', term)
    _post(
        access_token,
        'https://api.quizlet.com/2.0/sets/' + str(set_id) + '/terms',
        data = {
            'term': term,
            'definition': definition
        },
        code = 201
    )

def deleteTerm(access_token, set_id, term_id):
    print('deleting', set_id, term_id)
    _delete(
        access_token,
        'https://api.quizlet.com/2.0/sets/' + str(set_id) + '/terms/' + str(term_id),
        code = 204
    )
 

def _get(token, url, code=None):
    response = requests.get(
        url,
        headers = {'Authorization': 'Bearer ' + token}
    )
    _check_status(response, code)
    return response.json()

def _post(token, url, data, code=None):
    response = requests.post(
        url,
        headers = {'Authorization': 'Bearer ' + token},
        json = data
    )
    _check_status(response, code)

def _delete(token, url, code=None):
    response = requests.delete(
        url,
        headers = {'Authorization': 'Bearer ' + token}
    )
    _check_status(response, code)

def _check_status(response, code=None):
    response.raise_for_status()
    if code and response.status_code != code:
        raise Exception(response.status_code, response.reason)
