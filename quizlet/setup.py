from setuptools import setup

setup(
    name='3DQuizlet',
    version='1.0',
    description='Python wrapper for Quizlet',
    url='https://gitlab.com/martizih/3Dquizlet',
    author='Martin Zihlmann',
    author_email='martizih@student.ethz.ch',
    license='MIT',
    packages=['quizlet'],
    install_requires=[
        'requests'
    ],
    zip_safe=False
)